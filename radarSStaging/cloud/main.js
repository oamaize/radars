
require('cloud/app.js');
var Image = require("parse-image");

// Require and initialize the Twilio module with your credentials
var client = require('twilio')('AC225b70ca89ff0255156405cdb2fa8b67', '85308f03cb7ef6f6260954a556d3a2b3');
var twilioNumber = '+16093575019';


var Devices = Parse.Object.extend("Devices");
var Messages = Parse.Object.extend("Messages");
var Blocks = Parse.Object.extend("Block");
var Report = Parse.Object.extend("Report");
var TestDevices = Parse.Object.extend("TestDevices");






// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
Parse.Cloud.define("hello", function(request, response) {
                   response.success("Hello world!");
                   });

Parse.Cloud.define("createUser", function(request, response){
                   
                   
                   Parse.User.logIn("admin", "8FE6F200-9432-4C74-82D1-9D27A50CFE91", {
                                    success: function(user) {
                                    
                                    var device = new Devices();
                                    device.set("firstName", request.params.firstName);
                                    device.set("lastName", request.params.lastName);
                                    if(request.params.phoneNumber){ device.set("phoneNumber", request.params.phoneNumber); }
                                    device.set("gender", request.params.gender);
                                    if(request.params.picture){ device.set("picture", request.params.picture); }
                                    device.set("isAvailable", true);
                                    device.set("forceSync", false);
                                    device.set("numberVerified", "0");
                                    device.set("reportCount", 0);
                                    device.set("messageCountFriend", 0);
                                    device.set("messageCountLocal", 0);
                                    
                                    var point = new Parse.GeoPoint({latitude: 0, longitude: 0});
                                    if(request.params.latitude && request.params.longitude){
                                        point = new Parse.GeoPoint({latitude: request.params.latitude, longitude: request.params.longitude});
                                    }
                                    device.set("location", point);
                                    device.set("isBlocked", false);
                                    
                                    device.set("pin", makePin());
                                    
                                    device.save(null, {
                                                success: function(device){
                                                
                                                if(request.params.phoneNumber){
                                                    Parse.Cloud.run('purgeDuplicates', {phoneNumber:request.params.phoneNumber, pin:device.get('pin')},{});
                                                }
                                                
                                                alert('New user created: ' + device.id);
                                                response.success(device);
                                                },
                                                error: function(device, error){
                                                response.error("Failed to create user");
                                                alert('Failed to create new user, with error code: ' + error.description);
                                                }
                                                });
                                    },
                                    error: function(user, error) {
                                        alert('Failed to login with admin: ' + error.description);
                                    }
                                    });
                   });

function makePin()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    
    for( var i=0; i < 10; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    
    return text;
}

Parse.Cloud.define("updateUserInfo", function(request, response){
                   
                   var query = new Parse.Query(Devices);
                   query.equalTo("phoneNumber", request.params.phoneNumber);
                   query.find({
                              success: function(results){
                                results[0].set("firstName", request.params.firstName);
                                results[0].set("lastName", request.params.lastName);
                                results[0].save(null, {
                                              success: function(){
                                              alert('Updated User info..');
                                              response.success(results[0]);
                                              },
                                              error: function(error){
                                              alert('Failed to update user info: ' + error.description);
                                              }
                                              });
                              },
                              error: function(error){
                              response.error("Failed to find user to update");
                              alert('Failed to find user to update, with error code: ' + error.description);
                              }
                   });
                   });

Parse.Cloud.define("updateUserInfoAndNotifyFriends", function(request, response){
                   
                   var query = new Parse.Query(Devices);
                   query.equalTo("phoneNumber", request.params.phoneNumber);
                   query.find({
                              success: function(results){
                              results[0].set("firstName", request.params.firstName);
                              results[0].set("lastName", request.params.lastName);
                              results[0].save(null, {
                                              success: function(){
                                              alert('Updated User info..');
                                              if(request.params.friendsArray){
                                                Parse.Cloud.run('notifyFriends', {friendsArray:request.params.friendsArray},{});
                                              }
                                              response.success(results[0]);
                                              },
                                              error: function(error){
                                              alert('Failed to update user info: ' + error.description);
                                              }
                                              });
                              },
                              error: function(error){
                              response.error("Failed to find user to update");
                              alert('Failed to find user to update, with error code: ' + error.description);
                              }
                              });
                   });

Parse.Cloud.define("notifyFriends", function(request, response){
                   
                   var friendsQuery = new Parse.Query(Devices);
                   var friendsArray = request.params.friendsArray.split(',');
                   for(var i = 0; i < friendsArray.length; i++){
                   friendsQuery.equalTo("phoneNumber", friendsArray[i]);
                   friendsQuery.find({
                                     success: function(results){
                                     results[0].set("forceSync", true);
                                     results[0].save(null, {
                                                     success: function(friend){
                                                     alert('Set forceSync to true for.. ' + friend.get("phoneNumber"));
                                                     },
                                                     error: function(error){
                                                     alert('Failed to save forceSync: ' + error.description);
                                                     }
                                                     });
                                     },
                                     error: function(error){
                                     alert('Failed to find user to save forceSync: ' + error.description);
                                     }
                                     });
                   }
                   });


Parse.Cloud.define("receiveSMS", function(request, response) {
                   console.log("Received a new text from: " + request.params.From);
                   console.log("Text: " + request.params.Body);
                   
                   
                   //TO DO: Push notify client that pin is confirmed with number
                   
                   //normalize US number for storage into device class
                   var phoneNumber = request.params.From;
                   if(request.params.From.indexOf("+1") != -1){
                    phoneNumber = request.params.From.replace("+1","");
                   }
                   
                   //ignore long messages or malformed messages
                   var body = request.params.Body;
                   
                   if(body.length < 100 && body.indexOf("verify your number: ") != -1){
                        var pin = body.substring(body.indexOf("verify your number: ")+20, body.indexOf("verify your number: ")+30);
                   
                        var pushQuery = new Parse.Query(Parse.Installation);
                        var query = new Parse.Query(Devices);
                        query.equalTo("pin",pin);
                        query.find({
                              success: function(results){
                              if(results[0] != null){
                              results[0].set("phoneNumber", phoneNumber);
                              results[0].set("numberVerified", "1");
                              results[0].save(null, {
                                              success: function(device){
                                              alert('Phone Number verified... ' + device.id);
                                              
                                              Parse.Cloud.run('purgeDuplicates', {phoneNumber:phoneNumber, pin:device.get('pin')},{});
                                              
                                              pushQuery.equalTo("pin", device.get('pin'));
                                              Parse.Push.send({
                                                              where: pushQuery,
                                                              data: {
                                                              alert: "Your phone number has been verified.",
                                                              type: "verification",
                                                              phoneNumber: phoneNumber,
                                                              }
                                                              },{
                                                              success: function(){
                                                              alert("receive SMS: Push was successful!");
                                                              response.success();
                                                              },
                                                              error: function(error){
                                                              response.error("Error in receive SMS push...");
                                                              alert("Error in sending receive SMS push: " + error.code + " " + error.message);
                                                              }
                                                              });
                                              response.success();
                                              },
                                              error: function(error){
                                              alert('Phone Verification failed..');
                                              }
                                              });
                                   }
                              },
                              error: function(error){
                              alert("Error: " + error.code + " " + error.message);
                              response.error("receiveSMS pin query failed..");
                              }
                              });
                   }else{
                   alert("text message malformed...");
                   response.error();
                   }
                   
                   });

Parse.Cloud.define("purgeDuplicates", function(request, response)
{
    //check for duplicate entry and remove before responding to call
    var deviceQuery = new Parse.Query(Devices);
    deviceQuery.equalTo('phoneNumber', request.params.phoneNumber);
    deviceQuery.notEqualTo('pin', request.params.pin);
    deviceQuery.find({
                     success: function(results){
                     if(results.length > 0){
                        for(var i = 0; i < results.length; i++){
                            results[i].destroy({
                                        success: function(){
                                        alert('matching device removed: ' + " " + request.params.phoneNumber + " pin: " + request.params.pin);
                                        },
                                        error: function(error){
                                        alert('unable to remove matching device, with error code: ' + error.description);
                                        }
                                        });
                        }
                     }else{
                        alert("didn't find any entries with matching numbers..");
                     }
                     },
                     error: function(){
                     alert('query for matching device failed, with error code: ' + error.description);
                     }
                     });
                   
                   });

Parse.Cloud.define("testPhoneVerification", function(request, response){
                   
                   var pin = request.params.pin;
                   var query = new Parse.Query(Devices);
                   
                   query.equalTo("pin", pin);
                   query.find({
                              success: function(results){
                                response.success(results[0]);
                              },
                              error: function(error){
                                alert("Error testing phone verification status: " + error.code + " " + error.message);
                              }
                   });
                   });

Parse.Cloud.define("findExistingUsers", function(request, response){
                   var query = new Parse.Query(Devices); // QUERY OVER MASTER TABLE: Need refactor..
                   
                   var addressBookArray = request.params.addressBook.split(',');
                   var myPhoneNumber = request.params.phoneNumber;
      
                   query.notEqualTo("phoneNumber", myPhoneNumber); //exclude self from search
                   query.containedIn("phoneNumber", addressBookArray);
                   query.find({
                               success: function(results){
                                var jsonResults = new Array();
                                for(var i = 0; i < results.length; i++){
                                    jsonResults[i] = results[i].toJSON();
                                    if(results[i].get("picture") != null){
                                        var fileUrl = results[i].get("picture").url();
                                        jsonResults[i].picture = fileUrl;
                                    }
                                    if(results[i].get("location") != null){
                                        var userLocation = results[i].get("location");
                                        jsonResults[i].location = userLocation.toJSON().latitude + "," + userLocation.toJSON().longitude;
                                    }
                    /********* Set Sync value for discovered Radar users  DO NOT DELETE!!!! ****/
                                        
                                    var recipientPhoneNumber = jsonResults[i].phoneNumber;
                                    Parse.Cloud.run('setSyncFlag', {recipientPhoneNumber:recipientPhoneNumber},{
                                                    success: function(){
                                                        alert("set sync value success");
                                                    },
                                                    error: function(error){
                                                        alert('Failed to set sync value, with error code: ' + error.description);
                                                    }
                                                    });
                                }
                                response.success(jsonResults);
                                },
                                error: function(error){
                                    alert("Error: " + error.code + " " + error.message);
                                    response.error("findExistingUsers failed");
                                }
                               });
                   });

Parse.Cloud.define("syncAddressBook", function(request, response){
                   var query = new Parse.Query(Devices); // QUERY OVER MASTER TABLE: Need refactor..
                   var myNameQuery = new Parse.Query(Devices);
                   
                   var addressBookArray = request.params.addressBook.split(',');
                   var myPhoneNumber = request.params.phoneNumber;
                   
                   myNameQuery.equalTo("phoneNumber", myPhoneNumber);
                   myNameQuery.find({
                                    success: function(results){
                                    if(results[0].get("forceSync") == true){
                                        results[0].set("forceSync", false);
                                        results[0].save(null, {
                                                    success: function(){
                                                    alert('Set forceSync to false..');
                                                    },
                                                    error: function(error){
                                                    alert('Failed to save forceSync update: ' + error.description);
                                                    }
                                                    });
                                    }
                                    
                                    query.notEqualTo("phoneNumber", myPhoneNumber); //exclude self from search
                                    query.containedIn("phoneNumber", addressBookArray);
                                    query.find({
                                               success: function(results){
                                               var jsonResults = new Array();
                                               for(var i = 0; i < results.length; i++){
                                               jsonResults[i] = results[i].toJSON();
                                               if(results[i].get("picture") != null){
                                               var fileUrl = results[i].get("picture").url();
                                               jsonResults[i].picture = fileUrl;
                                               }
                                               if(results[i].get("location") != null){
                                               var userLocation = results[i].get("location");
                                               jsonResults[i].location = userLocation.toJSON().latitude + "," + userLocation.toJSON().longitude;
                                               }
                                               }
                                               response.success(jsonResults);
                                               },
                                               error: function(error){
                                               alert("Error: " + error.code + " " + error.message);
                                               response.error("findExistingUsers failed");
                                               }
                                               });
                                    },
                                    error: function(error){
                                    alert("Error in getMyName call: " + error.code + " " + error.message);
                                    response.error("couldn't find this your name..");
                                    }
                                    });
                   
                   
                   });

Parse.Cloud.define("setSyncFlag", function(request, response){
                   //set Sync Flag
                   
                   var recipientPhoneNumber = request.params.recipientPhoneNumber;
                   var recipientSyncQuery = new Parse.Query(Devices);
                   
                   //set recipient's forceSync value to true so recipient knows to call syncAddressBook next time they launch app
                   recipientSyncQuery.equalTo("phoneNumber", recipientPhoneNumber);
                   recipientSyncQuery.find({
                                           success:function(results){
                                                if(results[0].get("forceSync") == false){
                                                    results[0].set("forceSync", true);
                                                    results[0].save(null, {
                                                                    success: function(){
                                                                    alert('Set forceSync to true for: ' + recipientPhoneNumber);
                                                                    },
                                                                    error: function(error){
                                                                    alert('Failed to save forceSync: ' + error.description);
                                                                    }
                                                                    });
                                                }
                                           },
                                           error:function(error){
                                           alert('Failed to find forceSync device: ' + error.description);
                                           }
                                           });
                   });

Parse.Cloud.define("sendMessage", function(request, response){
                   var messageObject = new Messages();
                   var senderPhoneNumber = request.params.sender;
                   var receiverPhoneNumber = request.params.receiver;
                   var message = request.params.message;
                   var senderName = request.params.senderName;
                   var imageUrl = request.params.imageUrl;
                   var receiverType = request.params.receiverType;
                   
                   messageObject.set("senderId", senderPhoneNumber);
                   messageObject.set("receiverId", receiverPhoneNumber);
                   messageObject.set("message", message);
                   messageObject.set("imageUrl", imageUrl);
                   messageObject.set("isRead", false);
                   messageObject.save(null, {
                                      success: function(messageObject){
                                                          
                                      alert('New message created: ' + messageObject.get("message"));
                                      
                                      var pushQuery = new Parse.Query(Parse.Installation);
                                      pushQuery.equalTo('phoneNumber', receiverPhoneNumber);
                                      
                                      if(receiverType == 1){
                                      Parse.Push.send({
                                                      where: pushQuery,
                                                      data: {
                                                      alert: "from " + senderName,
                                                      title: "from " + senderName,
                                                      badge: "Increment",
                                                      action: "com.radar.UPDATE_STATUS",
                                                      messageId: messageObject.id,
                                                      sound: "",
                                                      type: "message",
                                                      sender: senderPhoneNumber
                                                      }
                                                      },{
                                                      success: function(){
                                                      alert("pushText: Contact Push was successful!");
                                                      response.success(messageObject);
                                                      },
                                                      error: function(error){
                                                      response.error("Error sending Contact Push...");
                                                      alert("Error in sending Contact Push: " + error.code + " " + error.message);
                                                      }
                                                      });
                                      }else{
                                      Parse.Push.send({
                                                      where: pushQuery,
                                                      data: {
                                                      alert: "You just received a message from someone nearby!",
                                                      title: "You just received a message from someone nearby!",
                                                      badge: "Increment",
                                                      action: "com.radar.UPDATE_STATUS",
                                                      messageId: messageObject.id,
                                                      type: "message",
                                                      sound: "",
                                                      sender: senderPhoneNumber
                                                      }
                                                      },{
                                                      success: function(){
                                                      alert("pushText: Local Push was successful!");
                                                      response.success(messageObject);
                                                      },
                                                      error: function(error){
                                                      response.error("Error sending Local Push...");
                                                      alert("Error in sending Local Push: " + error.code + " " + error.message);
                                                      }
                                                      });
                                      }
                                      //response.success(messageObject);
                                      },
                                      error: function(messageObject, error){
                                      alert('Failed to create new message, with error code: ' + error.description);
                                      }
                                      });
                   
                   var query = new Parse.Query(Devices);
                   query.equalTo("phoneNumber", senderPhoneNumber);
                   query.find({
                              success: function(results){
                                if(receiverType == 1){
                                  results[0].set("messageCountFriend", results[0].get("messageCountFriend")+1);
                                }else{
                                  results[0].set("messageCountLocal", results[0].get("messageCountLocal")+1);
                                }
                              results[0].save(null, {
                                              success: function(){
                                              alert('incremented message count');
                                              },
                                              error: function(error){
                                              alert('Failed to increment message count: ' + error.description);
                                              }
                                              });
                              },
                              error: function(error){
                              alert("increment message count failed: " + error.code + " " + error.message);
                              }
                              });
                   
                   });

Parse.Cloud.define("getMessage", function(request, response){
                   var messageId = request.params.messageId;
                   var msgQuery = new Parse.Query(Messages);
                   
                   msgQuery.get(messageId,{
                                 success: function(result){
                                 var jsonObj = result.toJSON();
                                 
                                 if (result.get("imageUrl") != null){
                                 //var fileUrl = result.get("pictureMessage").url();
                                 jsonObj.pictureMessage = result.get("imageUrl");
                                 }
                                 response.success(jsonObj);
                                 },
                                 error: function(error){
                                 alert("Error getting unread message with error code: " + error.code + " " + error.message);
                                 response.error("get unread message failed..");
                                 }
                   });
                   });

Parse.Cloud.define("getUnreadMessages", function(request, response){
                   
                   var receiver = request.params.receiver;
                   var unreadMsgQuery = new Parse.Query(Messages);
                   var senderArray = request.params.senderString.split(',');
                   
                   unreadMsgQuery.containedIn("senderId", senderArray);
                   unreadMsgQuery.equalTo("receiverId", receiver);
                   unreadMsgQuery.equalTo("isRead", false);
                   unreadMsgQuery.ascending("createdAt");
                   
                   unreadMsgQuery.find({
                                       success: function(results){
                                       
                                       for(var i = 0; i < results.length; i++){
                                            var jsonObj = results[i].toJSON();
                                       
                                            if(results[i].get("imageUrl") != null){
                                                //var fileUrl = results[i].get("pictureMessage").url();
                                                jsonObj.pictureMessage = results[i].get("imageUrl");
                                            }
                                            results[i] = jsonObj;
                                       }
                                       response.success(results);

                                       },
                                       error: function(error){
                                            alert("Error getting unread msgs: " + error.code + " " + error.message);
                                            response.error("get Unread Messages failed");
                                       }
                                       });
                   });

Parse.Cloud.define("updateLocation", function(request, response){
                   
                   var point = new Parse.GeoPoint({latitude: request.params.latitude, longitude: request.params.longitude});
                   
                   var query = new Parse.Query(Devices); // QUERY OVER MASTER TABLE: Need refactor..
                   
                   query.equalTo("phoneNumber", request.params.phoneNumber);
                   query.find({
                              success: function(results){
                                results[0].set("location", point);
                                results[0].save(null, {
                                              success: function(){
                                                alert('Saved geoPoint');
                                                response.success();
                                              },
                                                error: function(error){
                                                alert('Failed to save geoPoint: ' + error.description);
                                              }
                                              });
                              response.success(results);
                              },
                              error: function(error){
                              alert("Error: " + error.code + " " + error.message);
                              response.error("updateLocation failed");
                              }
                              });
                    });

Parse.Cloud.define("getNearbyUsers", function(request, response){
                   
                   var point = new Parse.GeoPoint({latitude: request.params.latitude, longitude: request.params.longitude});
                   
                   var nearByQuery = new Parse.Query(Devices); // QUERY OVER MASTER TABLE: Need refactor..?
                   
                   var phoneNumber = request.params.phoneNumber;
                   
                   nearByQuery.notEqualTo("phoneNumber", phoneNumber); //exclude self from search
                   nearByQuery.equalTo("isAvailable", true);
                   //nearByQuery.near("location", point);
                   nearByQuery.withinMiles("location", point, 6000);
                   nearByQuery.limit(50);
                   nearByQuery.find({
                              success: function(results){
                                    var jsonResults = new Array();
                                    for(var i = 0; i < results.length; i++){
                                        jsonResults[i] = results[i].toJSON();
                                        jsonResults[i].firstName = "";
                                        jsonResults[i].lastName = "";
                                        jsonResults[i].pin = "";
                                        if(results[i].get("picture") != null){
                                            var fileUrl = results[i].get("picture").url();
                                            jsonResults[i].picture = fileUrl;
                                        }
                                    }
                                    response.success(jsonResults);
                              },
                              error: function(error){
                              alert("Error: " + error.code + " " + error.message);
                              response.error("get Nearby users failed");
                              }
                              });
                   });

Parse.Cloud.define("doNotDisturb", function(request, response){
                //set isAvailable field to false
                   
                   var phoneNumber = request.params.phoneNumber;
                   var deviceQuery = new Parse.Query(Devices);
                   deviceQuery.equalTo("phoneNumber", phoneNumber);
                   
                   deviceQuery.find({
                                    success: function(results){
                                    if(results[0].get("isAvailable") == true){
                                       results[0].set("isAvailable", false);
                                    }else{
                                        results[0].set("isAvailable", true);
                                    }
                                    results[0].save(null,{
                                                    success: function(){
                                                    alert('Is Available flag set..');
                                                    response.success();
                                                    },
                                                    error: function(error){
                                                    alert('Failed to set isAvailable flag, error code' + error.description);
                                                    }
                                                    })
                                    },
                                    error: function(error){
                                    alert('Failed to find device, error code' + error.description);
                                    }
                                    });
                   });

Parse.Cloud.define("getStatus", function(request, response){
                   
                   var deviceQuery = new Parse.Query(Devices);
                   
                   deviceQuery.equalTo("phoneNumber", request.params.phoneNumber);
                   
                   deviceQuery.find({
                                    success: function(results){
                                    var statusArray = { dnd:results[0].get("isAvailable"), forceSync: results[0].get("forceSync")};
                                    response.success(statusArray);
                                    //response.success(results[0].get("isAvailable"));
                                    },
                                    error: function(error){
                                    alert('Failed to find device, error code' + error.description);
                                    }
                                    });
                   });

Parse.Cloud.define("getSyncStatus", function(request, response){
                   
                   var deviceQuery = new Parse.Query(Devices);
                   
                   deviceQuery.equalTo("phoneNumber", request.params.phoneNumber);
                   
                   deviceQuery.find({
                                    success: function(results){
                                    response.success(results[0].get("forceSync").toString());
                                    },
                                    error: function(error){
                                    alert('Failed to find device, error code' + error.description);
                                    }
                                    });
                   });


Parse.Cloud.define("readMessageD", function(request, response){
                   var readMsgQuery = new Parse.Query(Messages);
                   
                   readMsgQuery.equalTo("objectId", request.params.messageObjectId);
                   readMsgQuery.find({
                                     success: function(results){
                                     results[0].destroy({
                                                     success: function(){
                                                     alert('Message read and destroyed: ' + request.params.messageObjectId);
                                                     response.success();
                                                     },
                                                     error: function(error){
                                                     alert('Failed to destroy message, error code: ' + error.description);
                                                     }
                                                     });
                                     },
                                     error: function(error){
                                     alert("Error sending read & destroyed notification: " + error.code + " " + error.message);
                                     response.error("read MessageD notification failed.");
                                     }
                                     });
                   });

Parse.Cloud.define("reportUser", function(request, response){
                    var report = new Report();
                    var deviceQuery = new Parse.Query(Devices);
                    deviceQuery.equalTo("phoneNumber", request.params.reportee);
                   
    
                    report.set("reporter", request.params.reporter);
                    report.set("reportee", request.params.reportee);
    
                    report.save(null, {
                                success: function(report){
                
                                alert('New report entry created...');
                                //response.success(report);
                                },
                                error: function(report, error){
                                alert('Failed to report user, with error code: ' + error.description);
                                }
                                });
                   
                    deviceQuery.find({
                                    success: function(results){

                                    results[0].set("reportCount", results[0].get("reportCount")+1);
                                    
                                    results[0].save(null,{
                                                    success: function(){
                                                    alert('Report Count Incremented..');
                                                    //response.success();
                                                    },
                                                    error: function(error){
                                                    alert('Failed to increment report count, error code' + error.description);
                                                    }
                                                    })
                                    },
                                    error: function(error){
                                    alert('Failed to find device, error code' + error.description);
                                    }
                                    });

                    });

Parse.Cloud.job("shrinkProfiles", function(request, status) {
  Parse.Cloud.useMasterKey();
  var query = new Parse.Query(Devices);
  query.each(function(result){
    var imageUrl =  result.get("picture").url();
    Parse.Cloud.httpRequest({ url: imageUrl 
    }).then(function(response){
      var image = new Image();
      return image.setData(response.buffer);  
    }).then(function(image){
      return image.scale({ratio:0.25});
    }).then(function(image){
      return image.setFormat("PNG");
    }).then(function(image){
      return image.data();
    }).then(function(buffer){
      var base64 = buffer.toString("base64");
      var cropped = new Parse.File("thumbnail.png", {base64:base64});
      return cropped.save();
    }).then(function(cropped){
      result.set("picture", cropped);
      return result.save();
    });
  });
});

 

